import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { FeaturesComponent } from './features.component';
const featureRoutes: Routes = [
  { path: '', component: FeaturesComponent ,
  children: [
  {
    path: 'profile',
    loadChildren: './profile/profile.module#ProfileModule'
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule'
  },
]}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(featureRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class FeaturesRoutingModule { }
