import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../service/login.service';
import { Employee } from '../models/employee';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private loginservice: LoginService, private employee: Employee) { }


  ngOnInit() {

}

  onLogin(): void {
    this.loginservice.getEmployees()
    .subscribe(employeesData => this.employee = employeesData);
    if (this.employee.userid !== 0 && this.employee.userid != null) {
    window.localStorage.setItem('isAuthenticated', 'true');
    this.router.navigate(['/home']);
    } else {
      this.router.navigate(['/login']);
    }
  }

}
