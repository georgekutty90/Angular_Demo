import { Injectable } from '@angular/core';
import { Router, Route, CanActivateChild, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthService implements CanActivateChild, CanActivateChild, CanLoad {

    constructor(private router: Router) { }

    canLoad(route: Route) {
        if (window.localStorage.getItem('isAuthenticated')) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (window.localStorage.getItem('isAuthenticated')) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.canActivate(route, state);
    }
}
