import { NgModule } from '@angular/core';
import { PreloadAllModules, Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { AuthService } from './core/auth.service';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login', },
  {
    path: 'home',
    loadChildren: './features/features.module#FeaturesModule',
    canLoad: [AuthService],
    canActivate: [AuthService],
    canActivateChild: [AuthService]
  },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthService
  ]
})
export class AppRoutingModule { }
