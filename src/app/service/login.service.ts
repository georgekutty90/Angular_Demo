import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Employee } from '../models/employee';

@Injectable()
export class LoginService {
  constructor(private http: Http ) { }
  getEmployees(): Observable<Employee> {
    return this.http.get('http://localhost:24535/api/employees')
        .map((response: Response) => <Employee>response.json());
}
  }
